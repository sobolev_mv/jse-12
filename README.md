## Task Manager - SSH

#### 1. Требования к  SOFTWARE

- JDK 11

#### 2. Стек технологий

- Maven

#### 3. Разработчик
Name: Sobolev Mikhail

Email: sobolev_mv@nlmk.com

#### 4. Команды для сборки приложения
```mvn clean``` - Удаление всех созданных в процессе сборки артефактов

```mvn compile``` - Компилирование проекта

```mvn test``` - Тестирование с помощью JUnit тестов

```mvn install``` - Копирование .jar (war , ear) в локальный репозиторий

```mvn deploy``` - Публикация файла в удалённый репозиторий


#### 5. Команды для запуска приложения

Команда для запуска приложения:
```
java -jar task-manager-1.0.0.jar
```  

Терминальные команды:

* help - Отображение списка терминальных команд;
* version - Отображение версии программы;
* about - Отображение информации об разработчике.
* exit - Завершение работы приложения.

* project-create - Создание нового проекта.
* projects-clear - Очистка списка проектов.
* projects-list - Отображение списка проектов.
* project-view-by-index - Отображение х-тик проекта по индексу.
* project-view-by-id - Отображение х-тик проекта по id.
* project-remove-by-name - Удаление проекта из списка по имени.
* project-remove-by-id - Удаление проекта из списка по id.
* project-remove-by-index - Удаление проекта из списка по индексу.
* project-update-by-index - Редактирование х-тик проекта по индексу.
* project-update-by-id - Редактирование х-тик проекта по id.

* task-create - Создание новой задачи.
* tasks-clear - Очистка списка задач.
* tasks-list - Отображение списка задач.
* task-view-by-index - Отображение х-тик задачи по индексу.
* task-view-by-id - Отображение х-тик задачи по id.
* task-remove-by-name - Удаление задачи из списка по имени.
* task-remove-by-id - Удаление задачи из списка по id.
* task-remove-by-index - Удаление задачи из списка по индексу.
* task-update-by-index - Редактирование х-тик задачи по индексу.
* task-update-by-id - Редактирование х-тик задачи по индексу по id.
* tasks-list-by-project-id - Отображение списка задач, привязанных к проекту по его id.
* task-add-to-project-by-ids - Добавление задачи к проекту по id.
* task-remove-form-project-by-ids - Удаление задачи из списка, призанного к проекту по id.

* user-create - Создание пользователя с ролью USER.
* admin-create - Создание пользователя с ролью ADMIN.
* users-clear - Очистка списка пользователей.
* users-list - Отображение списка пользователей.
* user-view-by-login - Отображение х-тик пользователя по логину.
* user-remove-by-login - Удаление из списка пользователя по логину.
* user-update-by-login - Редактирование х-тик пользователя по логину.
* user-view-by-id - Отображение х-тик пользователя по User Id.
* user-remove-by-id - Удаление из списка пользователя по User Id.
* user-update-by-id - Редактирование х-тик пользователя по User Id.
* user-sign-in - Регистрация пользователя.
* user-sign-out - Отмена регистрации пользователя.
* user-change-password - Смена пароля пользователя. 

Пример запуска приложения с теринальной командой:

```
java -jar task-manager-1.0.0.jar help
```

Пример пример клонирования проекта по протоколу SSH:

```
git clone git@gitlab.com:sobolev_mv/jse-05
```

#### 6. Ссылка на резервную копию проекта

```
https://github.com/sobolev-mv/jse
```