package ru.nlmk.sobolevmv.tm.service;

import ru.nlmk.sobolevmv.tm.entity.Task;
import ru.nlmk.sobolevmv.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class TaskService {

  private final TaskRepository taskRepository;

  public TaskService(final TaskRepository taskRepository) {
    this.taskRepository = taskRepository;
  }

  public Task create(final String name) {
    if (name == null || name.isEmpty()) return null;
    return taskRepository.create(name);
  }

  public Task create(final String name, final String description) {
    if (name == null || name.isEmpty()) return null;
    if (description == null || description.isEmpty()) return null;
    return taskRepository.create(name, description);
  }

  public Task update(final Long id, final String name, final String description) {
    if (id == null) return null;
    if (name == null || name.isEmpty()) return null;
    if (description == null || description.isEmpty()) return null;
    return taskRepository.update(id, name, description);
  }

  public void clear() {
    taskRepository.clear();
  }

  public Task findByIndex(final int index)  {
    if(index < 0 || index > taskRepository.getSize()) return null;
    return taskRepository.findByIndex(index);
  }

  public Task findByName(final String name) {
    if (name == null || name.isEmpty()) return null;
    return taskRepository.findByName(name);
  }

  public Task findById(final Long id) {
    if (id == null) return null;
    return taskRepository.findById(id);
  }

  public Task removeById(final Long id) {
    if (id == null) return null;
    return taskRepository.removeById(id);
  }

  public Task removeByName(final String name) {
    if (name == null || name.isEmpty()) return null;
    return taskRepository.removeByName(name);
  }

  public Task removeByIndex(final int index) {
    if(index < 0 || index > taskRepository.getSize()) return null;
    return taskRepository.removeByIndex(index);
  }

  public List<Task> findAll(Long userId) {
    return taskRepository.findAll(userId);
  }

  public List<Task> findAllByProjectId(final Long projectId, Long userId) {
    if (projectId == null) return Collections.emptyList();
    return taskRepository.findAllByProjectId(projectId, userId);
  }

}
